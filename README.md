# Test Supabase with  Next.js
Este proyecto esta creado unicamente con el fin de probar la autenticación de supabase.<br>
Hasta el momento todo esta bien, aún falta probar algunas cosas para poder continuar el desarrollo de la CMGarments (min ecommerce).

### Instrucciones para la ejecución del proyecto
- Creación del archivo .env.local con las siguientes variables
  - NEXT_PUBLIC_SUPABASE_URL - *(Url de tu proyecto en Supabase)*
  - NEXT_PUBLIC_SUPABASE_ANON_KEY - *(Anon key de tu proyecto en Supabes)*
  - NEXT_PUBLIC_TEST_LOGIN_EMAIL - *(Email registrado en la tabla users de supabase, con email confirmado o con la configuración del mismo deshabilitado)*
  - NEXT_PUBLIC_TEST_LOGIN_PASS - *(Contraseña del email)*
- Intalar las dependencias<br><br>
  ```sheel
  pnpm install
  ```
- Ejecucián de la aplicación utiliando el comando:<br><br>
  ```bash
  pnpm run dev
  ```
<br>

## Funcionalidad
Esta aplicación esta creada solo para probar Supabase por lo que la funcionalidad es básica.<br>
No incluye muchas páginas, sin embargo, si se protegen algunas con el fin de practicar la atentificación y protección de rutas. Las páginas disponibles en este mini proyecto son:
- Home (No protected)
- Login (No protected)
- Admin (Protected)

La protección de rutas esta hecha a través del middleware de Next.js de la siguiente forma.
```Typescript
import { createMiddlewareClient } from "@supabase/auth-helpers-nextjs";
import { NextRequest, NextResponse } from "next/server";

export async function middleware(req: NextRequest) {
  const res = NextResponse.next();
  const supabase = createMiddlewareClient({ req, res });

  const {
    data: { user },
  } = await supabase.auth.getUser();

  const pathname = req.nextUrl.pathname;

  //Redirect to admin page from login if user is authenticated
  if (user && pathname === '/login') {
    return NextResponse.redirect(new URL('/admin', req.url))
  };

  //Redirect to home page from admin if user is not authenticated
  if (!user && pathname === '/admin') {
    return NextResponse.redirect(new URL('/login', req.url))
  }
  return res
}

export const config = {
  matcher: ['/login', '/admin']
}
```
Debido a que la autentificación de Supabase te permite guardar la Cokkies ya no vi necesario utilizar librerias como next-auth ya que con el middleware de Next.js.

### Fetching de datos con Supabase
Proteger una tabla en supabase es muy intuitivo, te permite crear reglas que protegeran tu tabla de peticiones e insersiones de datos.<br>
Puedes crear reglas para permitir que solo puedan consultar datos si se tiene la API Key mas no se esta atenticado, también puedes añadir una regla para que consulten las personas autenticadas y aunque aún no lo he probado, tambien puedes crear reglas para proteger tu tabla basado en roles.<br>
En mi caso permito que cualquiera pueda consultar datos de la tabla **Category** aunque no esten atenticados pero deben de tener la Api Key. Este rol se llama Anon y la clave se configura en las variables de entorno como **NEXT_PUBLIC_SUPABASE_ANON_KEY**.
