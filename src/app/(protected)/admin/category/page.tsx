
import type { FC } from 'react';
import CategoryTable from './components/CategoryTable';

interface pageProps {}

const page: FC<pageProps> = ({}) => {
    return (
      <main className='main p-3'>
        <div className='bg-slate-700 p-3 rounded-md m-auto w-full max-w-4xl space-y-7'>
          <h1 className='text-4xl font-light'>🩳Categories table</h1>
          <CategoryTable />
        </div>
      </main>
    );
}
export default page;