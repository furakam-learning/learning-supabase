"use client";

import { supabaseClient } from "@/utils/supabase-client";
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { useState, type FC, useReducer, useEffect } from "react";

interface CategoryTableProps {}

type Category = {
  id: string;
  name: string;
  created_at: string;
};

const columnHelper = createColumnHelper<Category>();

const columns = [
  columnHelper.accessor("id", {
    cell: (info) => info.getValue(),
  }),
  columnHelper.accessor("name", {
    cell: (info) => info.getValue(),
  }),
  columnHelper.accessor("created_at", {
    cell: (info) => info.getValue(),
  }),
];

const CategoryTable: FC<CategoryTableProps> = ({}) => {
  const supabase = supabaseClient();

  const [data, setData] = useState<any[] | null>(() => []);

  const table = useReactTable({
    data: data!,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  useEffect(() => {
    supabase
      .from("Category")
      .select("*")
      .then(({ data: categories }) => {
        const newList: any[] | undefined = categories?.map((c) => {
          return {
            ...c,
            created_at: new Date(c.created_at!).toLocaleString("en-GB"),
          };
        });
        setData(newList!);
      });
  }, [supabase]);

  return (
    <div className="bg-slate-800 rounded-md p-2">
      <table className="table-auto w-full border-separate">
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr
              key={headerGroup.id}
              className="h-12 bg-slate-900"
            >
              {headerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map((row) => (
            <tr key={row.id} className="h-10 bg-slate-700">
              {row.getVisibleCells().map((cell) => (
                <td key={cell.id} className="text-center">
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
export default CategoryTable;
