"use client";

import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Link from "next/link";
import { useRouter } from "next/navigation";

export default function AdminPage() {
  const router = useRouter();

  const supabase = createClientComponentClient();

  const handleSignOut = async () => {
    await supabase.auth.signOut();
    router.refresh();
  };

  return (
    <main className="main">
      <div className="m-auto p-5 flex flex-col gap-8 bg-slate-700 rounded-md w-80 shadow-slate-900 shadow-lg">
        <h1 className="text-3xl font-light text-center">
          Hello From Admin Page!
        </h1>
        <div className="p-3 bg-slate-800 rounded-md space-y-4">
          <p className="text-blue-200 text-xl text-center">Opciones</p>
          <ul className="space-y-3">
            <li className="p-1 bg-blue-700 rounded-sm text-center">
              <Link href="/">Go to Home page</Link>
            </li>
            <li className="p-1 bg-blue-700 rounded-sm text-center">
              <Link href="/admin/category">Go to Category page</Link>
            </li>
          </ul>
        </div>
        <button
          className="p-2 bg-gradient-to-r from-red-300 to-amber-300 rounded-sm text-lg hover:brightness-105 transition duration-100 text-zinc-700 font-medium"
          onClick={handleSignOut}
        >
          SignOut
        </button>
      </div>
    </main>
  );
}
