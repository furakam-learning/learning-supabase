import { createServerComponentClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";
import Link from "next/link";

export default async function Home() {
  const supabase = createServerComponentClient({ cookies });

  const { data: {user}} = await supabase.auth.getUser();

  const { data: categories } = await supabase.from("Category").select("*");

  return (
    <main className="main">
      <div className="m-auto bg-slate-700 shadow-slate-900 shadow-lg p-5 rounded-md flex flex-col gap-5 max-w-md">
        <h1 className="text-transparent bg-clip-text bg-gradient-to-r from-orange-400 to-red-400 text-5xl font-medium">
          Hello world!
        </h1>
        <p className="text-zinc-200">
          This is a home page for this app. I test the supabase authentication.
          So far i have been able to solve all the problems that have arisen.
          <br></br>
          Next, you can see the fetched the supabase&apos;s data of Category
          table
        </p>
        <div className="w-full h-autho bg-gradient-to-r from-teal-400 to-blue-600 p-1 rounded-md">
          <ul className="bg-slate-700 rounded-md flex flex-col gap-3 p-2">
            {categories ? (
              categories.map((c) => {
                return (
                  <li
                    className="bg-slate-800 p-2 rounded-md text-center cursor-pointer hover:brightness-110"
                    key={c.id}
                  >
                    {c.name}
                  </li>
                );
              })
            ) : (
              <span className="loader border-4 border-teal-400 border-b-blue-500 m-auto my-10 h-10 w-10"></span>
            )}
          </ul>
          <div className="flex justify-center p-2">
            {
              user ?
              <Link href="/admin" className="text-white text-lg underline underline-offset-4 decoration-2  decoration-teal-200 hover:text-teal-100">Go to admin page</Link>
              :
              <Link href="/login" className="text-white underline underline-offset-2 decoration-orange-300">Go to login page</Link>
            }
          </div>
        </div>
      </div>
    </main>
  );
}
