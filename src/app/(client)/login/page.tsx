"use client";

import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import { useRouter } from "next/navigation";
import { useForm, SubmitHandler } from "react-hook-form";
import { Toaster, toast } from "sonner";

interface LoginInputs {
  email: string;
  password: string;
}

export default function LoginPage() {
  const router = useRouter();

  const supabase = createClientComponentClient();

  /* React form hook */
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isSubmitting },
  } = useForm<LoginInputs>();

  const onSubmit: SubmitHandler<LoginInputs> = async (data) => {
    const {
      data: { user },
      error,
    } = await supabase.auth.signInWithPassword({
      email: data.email,
      password: data.password,
    });

    if (error) {
      toast.error(error.message);
    }

    if (user) {
      reset();
      toast.success("Logeado correctamente");
    }
    router.refresh();
  };

  return (
    <main className="main">
      <Toaster richColors position="bottom-center" />
      <div className="m-auto p-5 flex flex-col gap-8 bg-slate-700 rounded-md w-80 shadow-slate-900 shadow-lg">
        <h1 className="text-3xl font-light text-center">Login page</h1>

        <form
          onSubmit={handleSubmit(onSubmit)}
          className="grid grid-cols-1 grid-rows-2 gap-4"
        >
          <div className="input-group">
            <label>Email</label>
            <input
              {...register("email", {
                required: "Este campo es requerido",
                minLength: {
                  value: 10,
                  message: "Debe tener mínimo 10 caracteres",
                },
                maxLength: 60,
                pattern: {
                  value: /^[\w\.-]+@[\w\.-]+\.\w+$/,
                  message: "Debe ingresar un email",
                },
              })}
              type="text"
              className="input"
              placeholder="youremail@mail.com"
              disabled={isSubmitting}
            />
            <p className="input-error">{errors.email?.message}</p>
          </div>
          <div className="input-group">
            <label>Password</label>
            <input
              {...register("password", {
                required: "Este campo es requerido",
                minLength: {
                  value: 6,
                  message: "Debe tener mínimo 6 caracteres",
                },
                maxLength: 20,
              })}
              disabled={isSubmitting}
              type="password"
              className="input"
            />
            <p className="input-error">{errors.password?.message}</p>
          </div>
          <button
            type="submit"
            disabled={isSubmitting}
            className="p-2 bg-gradient-to-r from-teal-500 to-blue-700 rounded-sm text-white text-lg hover:brightness-105 transition duration-100 disabled:saturate-50"
            //onClick={handleSignIn}
          >
            {isSubmitting ? (
              <span className="loader border-2 border-teal-400 border-b-blue-500 m-auto w-5 h-5"></span>
            ) : (
              "SignIn"
            )}
          </button>
        </form>
      </div>
    </main>
  );
}
