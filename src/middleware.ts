import { createMiddlewareClient } from "@supabase/auth-helpers-nextjs";
import { NextRequest, NextResponse } from "next/server";

export async function middleware(req: NextRequest) {
  const res = NextResponse.next();
  const supabase = createMiddlewareClient({ req, res });

  const {
    data: { user },
  } = await supabase.auth.getUser();

  const pathname = req.nextUrl.pathname;

  //Redirect to admin page from login if user is authenticated
  if (user && pathname === '/login') {
    return NextResponse.redirect(new URL('/admin', req.url))
  };

  //Redirect to home page from admin if user is not authenticated
  if (!user && pathname.startsWith('/admin') ) {
    return NextResponse.redirect(new URL('/login', req.url))
  }
  return res
}

export const config = {
  matcher: ['/login', '/admin/:path*']
}
