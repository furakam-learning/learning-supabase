import { Database } from "@/types/supabase-database.types";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";

export const supabaseClient = () => createClientComponentClient<Database>()